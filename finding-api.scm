(use http-client)
(use medea)
(use regex)
(use vector-lib)

(define (replace-spaces str)
    (string-substitute " " "%20" str #t))

(define (my-read-string pt)
    (read-string #f pt))

(define (get-json url args api-obj) ; args: alist, api-obj: alist
    (let ([request-args (string-append (alist->get-params `((,(quote SERVICE-VERSION)      . "1.13.0")
                                                            (,(quote SERVICE-NAME)         . "FindingService")
                                                            (,(quote SECURITY-APPNAME)     . ,(alist-ref 'client-id (alist-ref 'ebay api-obj)))
                                                            (,(quote RESPONSE-DATA-FORMAT) . "JSON")))
                                       "&"
                                       (alist->get-params args))])
    (read-json (call-with-input-request (string-append url "?" request-args) #f my-read-string))))

(define (alist->get-params alst)
    (fold (lambda (x a) 
                (string-append a (if (string=? a "") "" "&") (if (string=? (cdr x) "") 
                                                                 (symbol->string (car x)) 
                                                                 (string-append (symbol->string (car x)) 
                                                                                "=" 
                                                                                (cdr x)))))
        "" alst))

(define (read-config path)
    (let* ([config-json (read-json (open-input-file path))])
       config-json))

(define (filter-alist->alist alst)
    (vector-fold (lambda (i a x)
                    (append a `((,(string->symbol (string-append "itemFilter(" (number->string i) ").name"))    . ,(symbol->string (car x))) 
                                (,(string->symbol (string-append "itemFilter(" (number->string i) ").value(0)")) . ,(cdr x))))) 
                 '() 
                 (list->vector alst)))

(define (search-by-keywords kws filters api-obj) ;kws: string, filters: alist, api-obj: alist
    (let ([args (append `((OPERATION-NAME . "findItemsByKeywords")
                          (REST-PAYLOAD   . "")
                          (keywords       . ,(replace-spaces kws)))
                        (filter-alist->alist filters))])
        (get-json "https://svcs.ebay.com/services/search/FindingService/v1" args api-obj)))

(define (get-results rsp) ;rsp: FindingService JSON response
    (alist-ref 'item (vector-ref (alist-ref 'searchResult (vector-ref (alist-ref 'findItemsByKeywordsResponse rsp) 0)) 0)))