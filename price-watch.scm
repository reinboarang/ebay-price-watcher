#!/usr/bin/csi -s
(include "finding-api.scm")
(use postgresql)
(use loops)

(define (read-watch-items path)
    (read-json (open-input-file path)))

(define (get-price item)
    (string->number (alist-ref '__value__ 
                               (vector-ref (alist-ref 'currentPrice 
                                                      (vector-ref (alist-ref 'sellingStatus 
                                                                             item) 0)) 0))))

(define (sleep-hours h)
    (sleep (* 60 60 h)))

(define (avg-price items)
    (vector-fold (lambda (i a x) 
                    (/ (+ a (get-price x)) 2)) 
                 (get-price (vector-ref items 0))
                 (vector-copy items 1)))

(define (update-item-entries items api-obj)
    (let ([db (connect (alist-ref 'sql api-obj))])
        ;(query db "USE $1;" (alist-ref 'dbname (alist-ref 'sql api-obj)))
        (vector-for-each (lambda (i x)
                            (let* ([resp (get-results (search-by-keywords (alist-ref 'keywords x) 
                                                                          (alist-ref 'filters x)
                                                                         api-obj))]
                                   [avg-price (avg-price resp)])
                            (query db (string-append "CREATE TABLE IF NOT EXISTS " (alist-ref 'name x) " (id         serial primary key, \
                                                                                                          human_name varchar(80), \
                                                                                                          price      float, \
                                                                                                          quantity   int, \
                                                                                                          ts         decimal(15,0));"))
                            (query db (string-append "INSERT INTO " (alist-ref 'name x) " (human_name, price, quantity, ts) VALUES (\'"
                                      (alist-ref 'human-name x) "\',"
                                      (number->string avg-price) ","
                                      (number->string (vector-length resp)) ","
                                      (number->string (current-seconds)) ");"))
                            (sleep 30)))
                         items)))

(define (main)
    (do-forever (print "Updating item entries...")
                (update-item-entries (read-watch-items "items.json") (read-config "config.json"))
                (sleep-hours 2)))

(main)